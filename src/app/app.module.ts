import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SomeElementComponent } from './some-element/some-element.component';
import { NestedComponent } from './some-element/nested/nested.component';

@NgModule({
  declarations: [
    AppComponent,
    SomeElementComponent,
    NestedComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [SomeElementComponent]
})
export class AppModule { }
