import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-some-element',
  templateUrl: './some-element.component.html',
  styleUrls: ['./some-element.component.css']
})
export class SomeElementComponent implements OnInit {

  @Input()
  coolNumber: Observable<number>;

  @Output()
  outSomething = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }
  removeCustomElement() {
    this.outSomething.emit('this is some cool event data!!!!!!!!');
  }
}
