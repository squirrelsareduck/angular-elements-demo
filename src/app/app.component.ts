import {Component, Injector} from '@angular/core';
import {createCustomElement} from '@angular/elements';
import {SomeElementComponent} from './some-element/some-element.component';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'elements';
  _someNumber$: BehaviorSubject<number> = new BehaviorSubject(7);
  someNumber$ = this._someNumber$.asObservable();

  constructor(private injector: Injector) {
    const someElement = createCustomElement(SomeElementComponent, {injector});
    window.customElements.define('some-element', someElement);
  }

  addSomeElement() {
    const someHtmlElement = window.document.createElement('some-element') as any;
    someHtmlElement.coolNumber = this.someNumber$;
    someHtmlElement.addEventListener('mouseover', () => this._someNumber$.next(this._someNumber$.value + 1));
    someHtmlElement.addEventListener('outSomething', data => {
      console.log(`This is what our custom event's data looks like`, data);
      window.document.body.removeChild(someHtmlElement);
    });
    window.document.body.appendChild(someHtmlElement);
  }
}
